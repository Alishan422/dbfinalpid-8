﻿using Final.Customers;
using Final.Products;
using Final.Reports;
using Final.Sales_and_Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Final
{
    public partial class Admin_Form : Form
    {
        public Admin_Form()
        {
            InitializeComponent();
        }

        private void AddPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            panel8.Controls.Clear();
            panel8.Controls.Add(usercontrol);
            usercontrol.BringToFront();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProductMenue prdMenue = new ProductMenue();
            AddPanel(prdMenue);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Customer_Menue custMenue = new Customer_Menue();
            AddPanel(custMenue);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Reports_Menue report_Menue = new Reports_Menue();
            AddPanel(report_Menue);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Sales_and_transaction_menue sales_transaction_report_Menue = new Sales_and_transaction_menue();
            AddPanel(sales_transaction_report_Menue);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
