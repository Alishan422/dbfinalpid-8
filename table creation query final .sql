/*CREATE TABLE product (
    ProductId INT PRIMARY KEY Not Null,
    Product_ModelName VARCHAR(100),
    Product_Category VARCHAR(50),
    Product_Size VARCHAR(20),
    Product_Price DECIMAL(10, 2)
);

*/

/*
CREATE TABLE Customer (
    CustomerId INT PRIMARY KEY NOT NULL,
    Customer_FirstName VARCHAR(50),
    Customer_LastName VARCHAR(50),
    Customer_Gender CHAR(10),
    Customer_Address VARCHAR(100),
    Customer_ContactEmail VARCHAR(100)
	Customer_country VARCHAR(50);
	Customer_Region VARCHAR (50);
);

*/

/*
CREATE TABLE Supplier (
    SupplierId INT PRIMARY KEY NOT NULL,
    Supplier_Name VARCHAR(100),
    Supplier_Contact VARCHAR(50),
    Supplier_Address VARCHAR(100)
);

*/

/*
CREATE TABLE orders (
    OrderId INT PRIMARY KEY Not Null,
    CustomerId INT NOT NULL,
    OrderDate DATE,
    FOREIGN KEY (CustomerId) REFERENCES Customer(CustomerId)
);

*/

/*
CREATE TABLE Supplier_product (
    SupplierProductId INT PRIMARY KEY,
    SupplierId INT,
    ProductId INT,
    ProductQuantity INT,
    ProductPrice DECIMAL(10, 2),
    SuppliedDate DATE,
    FOREIGN KEY (SupplierId) REFERENCES Supplier(SupplierId),
    FOREIGN KEY (ProductId) REFERENCES product(ProductId)
);
*/

/*
Create TABLE Orderdetail (
    OrderDetailId INT PRIMARY KEY Not Null,
    ProductId INT,
    OrderId INT,
    Quantity INT,
    UnitPrice DECIMAL(10, 2),
    TotalPrice DECIMAL(10, 2),
    FOREIGN KEY (ProductId) REFERENCES product(ProductId),
    FOREIGN KEY (OrderId) REFERENCES orders(OrderId)
);

*/

/*
CREATE TABLE Customer_Transaction (
    TransactionId INT PRIMARY KEY Not Null,
    OrderDetailId INT,
    PaymentMethod VARCHAR(50),
    FOREIGN KEY (OrderDetailId) REFERENCES Orderdetail(OrderDetailId)
);
*/



/*
CREATE TABLE Product_Review (
    ReviewId INT PRIMARY KEY NOT NULL,
    ProductId INT,
    CustomerId INT,
    ReviewComment VARCHAR(255),
    ReviewDate DATE,
    FOREIGN KEY (ProductId) REFERENCES product(ProductId),
    FOREIGN KEY (CustomerId) REFERENCES Customer(CustomerId)
);
*/

/*

CREATE TABLE Product_warranty (
    ProductWarrantyId INT PRIMARY KEY NOT NULL,
    ProductId INT,
    ProductWarrantyPeriod INT,
    WarrantyStartDate DATE,
    WarrantyExpiryDate DATE,
    FOREIGN KEY (ProductId) REFERENCES product(ProductId)
);

*/